export const getLastBookPerPage = (page, booksPerPage) => page * booksPerPage;

export const getFirstBookPerPage = (lastBookPerPage, booksPerPage) => lastBookPerPage - booksPerPage;

export const getCurrentBooks = (bookList, firstBookPerPage, lastBookPerPage) => bookList.slice(firstBookPerPage, lastBookPerPage);