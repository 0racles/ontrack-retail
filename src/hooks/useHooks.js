import { useState, useEffect } from 'react';
import queryString from 'query-string';
import axios from 'axios';
import { useHistory, useLocation } from 'react-router-dom';

import {getCurrentBooks, getFirstBookPerPage, getLastBookPerPage} from './helpers';

const useHooks = () => {
  // state and variable declarations
  const history = useHistory();
  const location = useLocation();
  const [booksPerPage] = useState(20);
  const [bookList, setBookList] = useState([]);
  const [isLoading, setLoading] = useState(false);
  const [fieldValue, setFieldValue] = useState('');
  const [isSubmitted, setSubmitted] = useState(false);

  const path = window.location.pathname;
  const initialQueryString = queryString.parse(location.search);
  const initialPageNumber = Number(initialQueryString.page) || 1;

  const [currentPage, setCurrentPage] = useState(initialPageNumber);

  const lastBookPerPage = getLastBookPerPage(1, booksPerPage);
  const firstBookPerPage = getFirstBookPerPage(lastBookPerPage, booksPerPage);
  const currentBooks = getCurrentBooks(bookList, firstBookPerPage, lastBookPerPage);

  const getPageIndex = (pageIndex) => setCurrentPage(pageIndex);

  useEffect(() => {
    const getBooks = async () => {
      const result = await axios.post('http://nyx.vima.ekt.gr:3000/api/books', {
        page: currentPage,
        itemsPerPage: 100,
        filters: [{ type: 'all', values: [fieldValue] }]
      });
      setLoading(true)
      const books = await result?.data?.books;
      setBookList(books);
      setSubmitted(false);
    }
     getBooks();
     setLoading(false);
    // eslint-disable-next-line
  }, [currentPage, isSubmitted]);

  useEffect(() => {
    if (currentPage > 0) {
      history.push(`${path}?pageNumber=${currentPage}`);
    }
  }, [currentPage, history, path]);

  const handleClick = (getPageIndex) => {
    setCurrentPage(getPageIndex);
  }

  const handleSelectNextPage = () => {
    setCurrentPage(currentPage + 1);
  }

  const handleSelectPrevPage = () => {
    setCurrentPage(currentPage - 1);
  }

  const getSearchValue = value => {
    setFieldValue(value);
  }

  const handleSubmit = e => {
    e.preventDefault();
    setCurrentPage(1);
    setSubmitted(true);
  }

  return {
    currentBooks,
    booksPerPage,
    getPageIndex,
    isLoading,
    bookList,
    handleClick,
    currentPage,
    handleSelectNextPage,
    handleSelectPrevPage,
    getSearchValue,
    handleSubmit
  }
}

export default useHooks;
