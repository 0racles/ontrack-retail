import { getLastBookPerPage, getFirstBookPerPage, getCurrentBooks } from './helpers';

describe('test helpers', () => {
  it('should getLastBookPerPage()', () => {
    const foo = 2;
    const bar = 20;
    expect(getLastBookPerPage(foo, bar)).toBe(40);
  });

  it('should getFirstBookPerPage()', () => {
    const foo = 40;
    const bar = 20;
    expect(getFirstBookPerPage(foo, bar)).toBe(20);
  });

  it('should getCurrentBooks()', () => {
    const books = [{
      id: 1,
      author_title: 'lorem ipsum',
      author_name: 'tea team',
      book_publication_date: '1/2/2019'
    },
    {
      id: 2,
      author_title: 'ipsum tatum',
      author_name: 'John Doe',
      book_publication_date: '4/5/2012'
    },
    {
      id: 3,
      author_title: 'tatum oniel',
      author_name: 'Ryu Joshi',
      book_publication_date: '9/6/2016'
    }
  ]
    const foo = 2;
    const bar = 3;
    const expectedResult = [{"author_name": "Ryu Joshi", "author_title": "tatum oniel", "book_publication_date": "9/6/2016", "id": 3}]
    expect(getCurrentBooks(books, foo, bar)).toEqual(expectedResult);
  });
});
