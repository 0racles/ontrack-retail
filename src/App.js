import React from 'react';
import './custom.scss';
import BooksPage from './components/BooksPage'
import PaginationComponent from './components/Pagination'
import useHooks from './hooks/useHooks';

function App() {
  const {
    currentBooks,
    booksPerPage,
    getPageIndex,
    isLoading,
    bookList,
    handleClick,
    currentPage,
    handleSelectNextPage,
    handleSelectPrevPage,
    getSearchValue,
    handleSubmit
  } = useHooks();

  return (
    <section>
      <BooksPage 
        books={currentBooks}
        isLoading={isLoading}
        getSearchValue={getSearchValue}
        handleSubmit={(e) => handleSubmit(e)}
      />

      <PaginationComponent
        totalPages={bookList.length}
        booksPerPage={booksPerPage}
        getPageIndex={getPageIndex}
        handleClick={handleClick}
        currentPage={currentPage}
        handleSelectNextPage={handleSelectNextPage}
        handleSelectPrevPage={handleSelectPrevPage}
      />
    </section>
  );
}

export default App;
