import React from 'react'
import PropTypes from 'prop-types'
import { InputGroup, Form, FormControl, Container, Button } from 'react-bootstrap';

const Search = ({ getSearchValue, handleSubmit }) => {

  return (
    <Container>
      <Form onSubmit={(e) => handleSubmit(e)}>
        <InputGroup style={{ width: '100%' }} className="mb-3">
          <FormControl
            placeholder="search by book page"
            aria-label="search books"
            aria-describedby="search"
            onChange={(e => getSearchValue(e.target.value))}
          />
          <InputGroup.Append>
            <Button variant="primary" type="submit">
              Search
            </Button>
          </InputGroup.Append>
        </InputGroup>
      </Form>
    </Container>
  )
}

Search.propTypes = {
  handleSubmit: PropTypes.func,
  getSearchValue: PropTypes.func
}

export default Search
