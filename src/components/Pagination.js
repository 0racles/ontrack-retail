import React from 'react'
import PropTypes from 'prop-types'
import { Container, Row, Pagination } from 'react-bootstrap';

const PaginationComponent = ({ totalPages, booksPerPage, handleClick, currentPage, handleSelectNextPage, handleSelectPrevPage }) => {
  let items = [];
  for (let i = 1; i <= Math.ceil(totalPages/booksPerPage); i++) {
    items.push(
      <>
      <Pagination.Item key={i} active={i === currentPage} onClick={() => { handleClick(i)}}>
        {i}
      </Pagination.Item>
      </>
    );
  }

  return (
    <Container>
      <Row style={{ justifyContent: 'center'}}>
        <Pagination className="justify-content-md-center" size="md">
          <Pagination.Prev size="md" onClick={handleSelectPrevPage} />
          {items}
          <Pagination.Next size="md" active={currentPage > 5} onClick={handleSelectNextPage} />
        </Pagination>
      </Row>
    </Container>
  )
}

PaginationComponent.propTypes = {
  getPageIndex: PropTypes.func,
  totalPages: PropTypes.number,
  booksPerPage: PropTypes.number
}

export default PaginationComponent
