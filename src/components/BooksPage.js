import React from 'react'
import PropTypes from 'prop-types'
import { Card, Container, Row, Spinner, Jumbotron } from 'react-bootstrap';
import Search from './Search'

const BooksPage = ({ books, isLoading, getSearchValue, handleSubmit }) => {
  return (
    <Container>
      <Jumbotron className="bg-primary text-white text-center">
        <h2>Book List</h2>
      </Jumbotron>

      <Search getSearchValue={getSearchValue} handleSubmit={(e) => handleSubmit(e)} />

      <Row style={{ marginLeft: '0px', justifyContent: 'space-between' }}>
        {/* show loading when the when fetching books data */}
        {!isLoading ? (
          <Spinner animation="border" role="status" variant='primary' style={{ margin: '15px auto' }}>
            <span className="sr-only">Loading...</span>
          </Spinner>
        ):(
          <>
          {
            books.map(book => (
              <Card key={book.id} style={{ maxWidth: '330px', marginRight: '15px', marginBottom: '15px', width: '100%' }}>
                <Card.Body>
                  <Card.Title>Book Title: {book.book_title}</Card.Title>
                  <Card.Text>
                    Book Title: {book.book_title}
                  </Card.Text>

                  <Card.Text>
                    Book author: {book.book_author}
                  </Card.Text>

                  <Card.Text>
                    Book pages: {book.book_pages}
                  </Card.Text>

                  <Card.Text>
                    Book Publication Year: {book.book_publication_year}
                  </Card.Text>

                  <Card.Text>
                    {book.book_publication_country}: {book.book_publication_country}
                  </Card.Text>

                  <Card.Text>
                    Book Publication City: {book.book_publication_city}
                  </Card.Text>
                </Card.Body>
              </Card>
              )
            )
          }
        </>
        )}
      </Row>
    </Container>
  )
}

BooksPage.propTypes = {
  books: PropTypes.array,
  isLoading: PropTypes.bool
}

export default BooksPage
