import Search from '../Search'

const Props = {
  handleSubmit: jest.fn(),
  getSearchValue: jest.fn()
}

describe('Search', () => {
  const component = shallow(<Search {...Props} />);

  it('should match snapshot', () => {
    expect(component).toMatchSnapshot();
  })
});
