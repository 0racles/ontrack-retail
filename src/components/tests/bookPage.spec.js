import BooksPage from '../BooksPage'

const Props = {
  books: [],
  isLoading: false
}

describe('BookPages', () => {
  const component = shallow(<BooksPage {...Props} />);

  it('should match snapshot', () => {
    expect(component).toMatchSnapshot();
  })
});
