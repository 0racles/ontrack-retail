import Pagination from '../Pagination'

const Props = {
  getPageIndex: jest.fn(),
  totalPages: 2,
  booksPerPage: 10
}

describe('Pagination', () => {
  const component = shallow(<Pagination {...Props} />);

  it('should match snapshot', () => {
    expect(component).toMatchSnapshot();
  })
});
