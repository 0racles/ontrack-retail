import { useState } from 'react'
import useHooks from '../../hooks';

Object.defineProperties(window, { location: { pathname: ''}})

jest.mock('react', () => ({
  useState: jest.fn(),
  useEffect: jest.fn()
}));

jest.mock('react-router-dom', () => ({
  useHistory: () => ({
    push: jest.fn(),
  }),
  useLocation: () => ({
    location: {
      pathname: "localhost:3000/example/path"
    }
  })
}));

jest.mock('../../helpers', () => ({
  getLastBookPerPage: jest.fn(() => 40),
  getFirstBookPerPage: jest.fn(() => 20),
  getCurrentBooks: jest.fn(() => [])
}));

const mockSetState = jest.fn();

describe('test hooks', () => {
  useState.mockImplementationOnce(() => [10, mockSetState]);
  useState.mockImplementationOnce(() => [[], mockSetState]);
  useState.mockImplementationOnce(() => [false, mockSetState]);
  useState.mockImplementationOnce(() => ['', mockSetState]);
  useState.mockImplementationOnce(() => [false, mockSetState]);
  useState.mockImplementationOnce(() => [1, mockSetState]);
  

  const {
    currentBooks,
    booksPerPage,
    getPageIndex,
    isLoading,
    bookList
  } = useHooks();

  it('should test isLoading', () => {
    expect(isLoading).toBe(false);
  });

  it('should test bookList', () => {
    expect(bookList).toEqual([]);
  });

  it('should test currentBooks', () => {
    expect(currentBooks).toEqual([]);
  });

  it('should test booksPerPage', () => {
    expect(booksPerPage).toEqual(20);
  });
});