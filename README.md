# Summary

This App queries a paginated endpoint and displays a list of books. It retrieves the first hundred books when first loaded and prints out 20 on each page. You can see more pages by using the `Next` button or return to the previous page by using the `Previous` button. You can also find a particular book(s) by using the search bar.
# Getting Started

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

`cd` into ontract-retail directory and install dependencies using `npm install`. See below the available scripts to start this project and run unit tests.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm run test`

Launches the test runner in the interactive watch mode.

### `Improvements that could be made`

This project could be improved if given more time in the following ways:

1. Totally avoiding the use of inline-styling as it increases style specificity and its likely to override rules from stylesheet.
2. Improve responsiveness. I placed less emphasis on page layout and aesthetics
3. Folder structure. I didn't bother much with folder structure due to the size of the project.
4. Add unhappy path.
5. Configure ESLint to make linting tighter.


